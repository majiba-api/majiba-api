# quelques notes
https://hackmd.io/nNmVm0YZT4q6hpmVvqe3mw?view

#run l'application en local


http://localhost:8080/h2-console

accéder a swagger

http://localhost:8080/swagger-ui.html

#construire l'image docker


mvn clean install

docker build -t pseudoDockerHub/nomImage:version .

docker login

docker push pseudoDockerHub/nomImage:version


#se connecter a manage.openshift.com


faire un pod containerPort 8080

faire un service avec selector label du pod précédent

faire une route

#exemple de déploiement:

http://majiba-api-majiba-api.apps.us-east-2.starter.openshift-online.com/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

