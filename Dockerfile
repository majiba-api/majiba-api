FROM tomcat:9.0.33

COPY target/majiba-api.war /usr/local/tomcat/webapps/ROOT.war

ADD ./data/entrant/liste_majibav2.csv /data/entrant/liste_majibav2.csv

EXPOSE 8080
EXPOSE 8443
CMD ["catalina.sh", "run"]
