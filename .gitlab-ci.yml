stages:
  - validate
  - test
  - build
  - docker
  - deploy

variables:
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-XX:MaxPermSize=512m -Xmx1536m -Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true -Dmaven.artifact.threads=1"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd`are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  #JAVA_HOME for the jdk
  JAVA_HOME: "/usr/lib/jvm/java-8-openjdk-amd64/"
  MAVEN_BIN: "mvn"
  GIT_SSL_NO_VERIFY: 1

validate-source:
  stage: validate
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS validate"
  image: maven:3.6.0-jdk-8

unit-test:
  stage: test
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS -V verify -P unittest"
  image: maven:3.6.0-jdk-8
  

integration-test:testcontainers:
  stage: test
  image: maven:3.6.0-jdk-8
  variables:
    GIT_SSL_NO_VERIFY: 1
    JAVA_HOME: /docker-java-home
    # Instruct Testcontainers to use the daemon of DinD.
    DOCKER_HOST: "tcp://docker:2375"
    # Improve performance with overlayfs.
    DOCKER_DRIVER: overlay2
  # DinD service is required for Testcontainers
  services:
    - docker:dind
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS  -V verify -P testcontainers-pg"

integration-test:gitlab-ci:
  stage: test
  image: maven:3.6.0-jdk-8
  variables:
    GIT_SSL_NO_VERIFY: 1
    POSTGRES_DB: integration-tests-db
    POSTGRES_USER: sa
    POSTGRES_PASSWORD: sa
    POSTGRES_HOST: postgres
  services:
    - postgres:12.2
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS  -V verify -P gitlab-ci"
    
package:jdk8:
  stage: build
  image: maven:3.6.0-jdk-8
  variables:
    GIT_SSL_NO_VERIFY: 1
    JAVA_HOME: /docker-java-home
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS  -V package -DskipTests"
  artifacts:
    paths:
      - target/*.war
    expire_in: 1 day



docker:
  stage: docker
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  services:
    - docker:19.03.1-dind
  image: docker:19.03.1
  script:
    - docker build --pull -t "donatien26/majiba-api:$CI_COMMIT_REF_SLUG" .
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
    - docker push "donatien26/majiba-api:$CI_COMMIT_REF_SLUG"

deploy review on kub:
  stage: deploy
  variables:
    APP_ID: majiba-api-$CI_COMMIT_REF_SLUG
    HAPROXY: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.demo.dev.sspcloud.fr
    REVIEW_IMAGE: donatien26/majiba-api:$CI_COMMIT_REF_SLUG
  image:
    name: thisiskj/kubectl-envsubst
    entrypoint: ["/bin/sh", "-c"]
  script:
    - envsubst < .ci/deployment-pg.yaml
    - envsubst < .ci/deployment-pg.yaml | kubectl --token $KUB_TOKEN --server $KUB_SERVER --namespace $KUB_NAMESPACE --insecure-skip-tls-verify=true replace --force -f -
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.demo.dev.sspcloud.fr
    on_stop: stop_review on kub

stop_review on kub:
  stage: deploy
  variables:
    GIT_STRATEGY: none
    APP_ID: majiba-api-$CI_COMMIT_REF_SLUG
  image:
    name: thisiskj/kubectl-envsubst
    entrypoint: ["/bin/sh", "-c"]
  script:
    - kubectl --token $KUB_TOKEN --server $KUB_SERVER --namespace $KUB_NAMESPACE --insecure-skip-tls-verify=true delete deployments $APP_ID
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
