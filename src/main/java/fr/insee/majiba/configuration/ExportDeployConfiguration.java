package fr.insee.majiba.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import fr.insee.majiba.service.export.ExportDeployService;
import fr.insee.majiba.service.export.MajibaCsvService;

@Configuration
public class ExportDeployConfiguration implements EnvironmentAware {

    private Environment environment;

    @Bean
    @ConditionalOnProperty(name = "fr.insee.majiba-api.export.type", havingValue = "csv", matchIfMissing = true)
    public ExportDeployService exportDeployServiceCsv() {
	return new MajibaCsvService(this.environment.getProperty("fr.insee.majiba-api.dossier.cei.racine"));
    }

    // Pour un jour
    // @ConditionalOnProperty(name="fr.insee.majiba-api.export.type",havingValue="json",
    // matchIfMissing=false)
    // public ExportDeployService exportDeployServiceJson() {
    // return new MajibaCsvService(this.environment.getProperty(key))
    // }

    @Override
    public void setEnvironment(Environment environment) {
	this.environment = environment;

    }
}
