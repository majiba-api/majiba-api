package fr.insee.majiba.configuration.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.insee.majiba.MajibaProperties;

@Configuration
@EnableWebSecurity
@ConditionalOnProperty(name = MajibaProperties.MAJIBA_API_SECURITY, havingValue = "basic")
public class BasicSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(BasicSecurityConfiguration.class);

    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	logger.info("adding in memory basic users");
	auth.inMemoryAuthentication().withUser("user").password(passwordEncoder().encode("password")).roles("USER")
		.and().withUser("admin").password(passwordEncoder().encode("admin")).roles("USER", "ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
	// tout en https
	http.authorizeRequests()
		// configuration pour endpoint sans sécurité Basic
        .antMatchers("/application/{\\d+}/deploy", "/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html","/config/load")
		.permitAll().anyRequest().authenticated().and().httpBasic();
	http.headers().frameOptions().disable();
	http.csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
	return new BCryptPasswordEncoder();
    }
}
