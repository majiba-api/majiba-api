package fr.insee.majiba.configuration.security;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import fr.insee.majiba.MajibaProperties;

@Configuration
@EnableWebSecurity
@ConditionalOnProperty(name = MajibaProperties.MAJIBA_API_SECURITY, havingValue = "nosecurity")
public class NoSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
	// tout en https
	http.authorizeRequests().antMatchers("/**").permitAll();
	http.headers().frameOptions().disable();
	http.csrf().disable();
    }
}
