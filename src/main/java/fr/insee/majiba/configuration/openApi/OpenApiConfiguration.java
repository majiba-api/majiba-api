package fr.insee.majiba.configuration.openApi;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.insee.majiba.MajibaProperties;

@Configuration
public class OpenApiConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(OpenApiConfiguration.class);

	@Value("${keycloak.auth-server-url:}")
	public String keycloakUrl;

	@Value("${keycloak.realm:}")
	public String realmName;

	@Autowired
	MajibaProperties majibaProperties;

	public final String SCHEMEKEYCLOAK = "oAuthScheme";
	public final String SCHEMEBASIC = "basic";

	@Bean
	@ConditionalOnProperty(name = "fr.insee.majiba-api.security", havingValue = "keycloak", matchIfMissing = true)
	public OpenAPI customOpenAPIKeycloak() {
		final OpenAPI openapi = createOpenAPI();
		openapi.components(new Components().addSecuritySchemes(SCHEMEKEYCLOAK, new SecurityScheme()
				.type(SecurityScheme.Type.OAUTH2).in(SecurityScheme.In.HEADER).description("Authentification keycloak")
				.flows(new OAuthFlows().authorizationCode(new OAuthFlow()
						.authorizationUrl(keycloakUrl + "/realms/" + realmName + "/protocol/openid-connect/auth")
						.tokenUrl(keycloakUrl + "/realms/" + realmName + "/protocol/openid-connect/token")
						.refreshUrl(keycloakUrl + "/realms/" + realmName + "/protocol/openid-connect/token")))));
		return openapi;
	}

	@Bean
	@ConditionalOnProperty(name = "fr.insee.majiba-api.security", havingValue = "nosecurity")
	public OpenAPI customOpenAPI() {
		final OpenAPI openapi = createOpenAPI();
		return openapi;
	}

	@Bean
	@ConditionalOnProperty(name = "fr.insee.majiba-api.security", havingValue = "basic")
	public OpenAPI customOpenAPIBasic() {
		final OpenAPI openapi = createOpenAPI();
		openapi.components(new Components().addSecuritySchemes(SCHEMEBASIC,
				new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme(SCHEMEBASIC).in(SecurityScheme.In.HEADER)
						.description("Authentification Basic").name("basicAuth")));
		return openapi;
	}

	private OpenAPI createOpenAPI() {
		logger.info("surcharge de la configuration swagger");
		final OpenAPI openapi = new OpenAPI()
				.info(new Info().title("swagger d'api majiba").description("swagger d'api majiba"));
		if (StringUtils.isNotBlank(majibaProperties.getSwaggerBasePathUrl())) {
			logger.info("surcharge de server avec {}" + majibaProperties.getSwaggerBasePathUrl());
			final List<Server> servers = new ArrayList<Server>();
			servers.add(new Server().url(majibaProperties.getSwaggerBasePathUrl()));
			openapi.setServers(servers);
		}
		return openapi;
	}

	@ConditionalOnProperty(name = "fr.insee.majiba-api.security", havingValue = "keycloak", matchIfMissing = true)
	@Bean
	public OperationCustomizer ajouterKeycloak() {
		return (operation, handlerMethod) -> {
			if (StringUtils.equalsIgnoreCase("NomDuEndpoint", handlerMethod.getMethod().getName())) {
				return operation;
			}
			return operation.addSecurityItem(new SecurityRequirement().addList(SCHEMEKEYCLOAK));
		};
	}

	@ConditionalOnProperty(name = "fr.insee.majiba-api.security", havingValue = "basic")
	@Bean
	public OperationCustomizer ajouterBasic() {
		return (operation, handlerMethod) -> {
			if (StringUtils.equalsIgnoreCase("NomDuEndpoint", handlerMethod.getMethod().getName())) {
				return operation;
			}
			return operation.addSecurityItem(new SecurityRequirement().addList(SCHEMEBASIC));
		};
	}
}