package fr.insee.majiba.util;

import org.apache.commons.lang3.StringUtils;

import fr.insee.majiba.domain.Module;

public class ModuleUtils {

    public final static String getModuleKey(Module module) {
	final String moduleName = StringUtils.isBlank(module.getName()) ? "" : module.getName();
	return moduleName + module.getTypeModule().getId();
    }
}
