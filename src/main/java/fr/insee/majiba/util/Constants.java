package fr.insee.majiba.util;

public class Constants {
    public final static String WEB_CONF = "web";
    public static final String PRODUCTION_PROPERTIES_PATH = "properties.path";

    // FIXME quand aura l'authentification
    public final static String SYSTEM = "SYSTEM";

}
