package fr.insee.majiba.mapper;

import org.springframework.stereotype.Component;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.view.ApplicationView;
import fr.insee.majiba.view.ModuleView;

@Component
public class ApplicationViewMapper implements Mapper<Application, ApplicationView> {

    @Override
    public ApplicationView map(Application app) {
	final ApplicationView applicationView = new ApplicationView();
	applicationView.setNom(app.getName());
	app.getModules().forEach(m -> {
	    final ModuleView moduleView = new ModuleView();
	    moduleView.setCode(m.getTypeModule().getCode());
	    moduleView.setName(m.getName());
	    applicationView.getModules().add(moduleView);
	});

	return applicationView;
    }
}
