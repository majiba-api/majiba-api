package fr.insee.majiba.mapper;

import org.springframework.stereotype.Component;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.view.DeploymentView;
import fr.insee.majiba.view.MajibaView;

@Component
public class MajibaViewMapper {

    private static final String PRIORITY_DEFAULT = "3";

    // @Value("${fr.insee.majiba-api.environnement}")
    // private List<String> listeEnvironnement;
    //
    // @Value("${fr.insee.majiba-api.plateforme}")
    // private List<String> listePlateforme;
    //
    // @Value("${fr.insee.majiba-api.typmaj}")
    // private List<String> listeTypeMaj;

    public MajibaView mapperHandly(Application application, DeploymentView deployment) {
	// TODO ICI on peut mettre la logique métier
	// A minima gérer les champs requis je me retrouve avec des null dans le
	// nom.
	final MajibaView majibaCsvView = new MajibaView();

	majibaCsvView.setUrgent(deployment.getPriority() != null ? deployment.getPriority().toString()
		: PRIORITY_DEFAULT);
	majibaCsvView.setApplication(application.getName());
	majibaCsvView.setModule(deployment.getModuleName());
	majibaCsvView.setVersion(deployment.getVersion());
	majibaCsvView.setTypemaj(deployment.getTypeMaj());
	majibaCsvView.setEnvironnement(deployment.getEnvironnement());
	majibaCsvView.setPlateforme(deployment.getPlateforme());
	majibaCsvView.setLienLivrable(deployment.getUrl());
	majibaCsvView.setMail(deployment.getMail());
	majibaCsvView.setValidQualif(Boolean.TRUE.toString());
	return majibaCsvView;
    }
    // trop stricte pour le moment
    // public void fieldsControl (DeploymentView deployment) throws
    // MajibaFieldErrorException {
    // if (!listeEnvironnement.contains(deployment.getEnvironnement()) ){
    // throw new MajibaFieldErrorException(deployment.getEnvironnement()) ;
    // }
    // if (!listePlateforme.contains(deployment.getPlateforme()) ){
    // throw new MajibaFieldErrorException(deployment.getPlateforme()) ;
    // }
    // if (!listeTypeMaj.contains(deployment.getTypemaj()) ){
    // throw new MajibaFieldErrorException(deployment.getTypemaj()) ;
    // }
    // }

}
