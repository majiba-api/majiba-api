package fr.insee.majiba.mapper;

public interface Mapper<T, S> {

    S map(T t);

}
