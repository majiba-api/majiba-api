package fr.insee.majiba.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.insee.majiba.domain.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long> {

    Application findByName(String name);

    @Query("select app from Application app where app.name=:name")
    Application findByNameWithModule(@Param("name") String name);

    @Query("select app.name from Application app")
    List<String> findAllNames();

}