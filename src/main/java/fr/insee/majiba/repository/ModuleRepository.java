package fr.insee.majiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.insee.majiba.domain.Module;

public interface ModuleRepository extends JpaRepository<Module, Long> {

}