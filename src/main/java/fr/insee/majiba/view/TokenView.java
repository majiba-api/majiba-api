package fr.insee.majiba.view;

public class TokenView {

    private String token;

    public String getToken() {
	return token;
    }

    public TokenView() {
	super();
    }

    public TokenView(String token) {
	this.token = token;
    }

    public void setToken(String token) {
	this.token = token;
    }

}
