package fr.insee.majiba.view;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MajibaView {

    @JsonProperty("application")
    private String application;
    @JsonProperty("module")
    private String module;
    @JsonProperty("version")
    private String version;
    @JsonProperty("typemaj")
    private String typemaj;
    @JsonProperty("urgent")
    private String urgent;
    @JsonProperty("environnement")
    private String environnement;
    @JsonProperty("lienlivrable")
    private String lienLivrable;
    @JsonProperty("plateforme")
    private String plateforme;
    @JsonProperty("mail")
    private String mail;
    @JsonProperty("validqualif")
    private String validQualif;
    @JsonProperty("nom_fichier_log")
    private String nomFichierLog;
    

    public MajibaView() {
	super();
    }

    public String getApplication() {
	return application;
    }

    public void setApplication(String application) {
	this.application = application;
    }

    public String getModule() {
	return module;
    }

    public void setModule(String module) {
	this.module = module;
    }

    public String getVersion() {
	return version;
    }

    public void setVersion(String version) {
	this.version = version;
    }

    public String getTypemaj() {
	return typemaj;
    }

    public void setTypemaj(String typemaj) {
	this.typemaj = typemaj;
    }

    public String getLienLivrable() {
	return lienLivrable;
    }

    public void setLienLivrable(String lienLivrable) {
	this.lienLivrable = lienLivrable;
    }

    public String getMail() {
	return mail;
    }

    public void setMail(String mail) {
	this.mail = mail;
    }

    public String getValidQualif() {
	return validQualif;
    }

    public void setValidQualif(String validQualif) {
	this.validQualif = validQualif.toString();
    }

    public String getUrgent() {
	return urgent;
    }

    public void setUrgent(String urgent) {
	this.urgent = urgent;
    }

    public String getEnvironnement() {
	return environnement;
    }

    public void setEnvironnement(String environnement) {
	this.environnement = environnement;
    }

    public String getPlateforme() {
	return plateforme;
    }

    public void setPlateforme(String plateforme) {
	this.plateforme = plateforme;
    }

	public String getNomFichierLog() {
		return nomFichierLog;
	}

	public void setNomFichierLog(String nomFichierLog) {
		this.nomFichierLog = nomFichierLog;
	}

}