package fr.insee.majiba.view;

public class MessageView {

    private String message;

    public MessageView() {
	super();
    }

    public MessageView(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    };

    public void setMessage(String message) {
	this.message = message;
    };

}
