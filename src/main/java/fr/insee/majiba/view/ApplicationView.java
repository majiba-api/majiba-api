package fr.insee.majiba.view;

import java.util.HashSet;
import java.util.Set;

public class ApplicationView {

    private String nom;

    private final Set<ModuleView> modules = new HashSet<>();

    public String getNom() {
	return nom;
    }

    public void setNom(String nom) {
	this.nom = nom;
    }

    public Set<ModuleView> getModules() {
	return modules;
    }

    public ApplicationView() {
	super();
    }

}
