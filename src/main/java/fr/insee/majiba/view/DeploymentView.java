package fr.insee.majiba.view;

import io.swagger.v3.oas.annotations.media.Schema;

public class DeploymentView {

	@Schema(description = "nom du module", example = "elire-bach", required = true)
	// @NotBlank
	private String moduleName;

	// todo on le met ds la descript du shcema ?
	// 1 : Mise à jour “urgente” : ferme les couloirs un à un sans attendre la
	// déconnexion du dernier utilsateur
	// 2: Mise à jour “immédiate” : ferme le couloir pour toute nouvelle connexion
	// mais attends le délai par défaut (1200 secondes) que le dernier utilsateur
	// soit déconnecté
	// 3 : Mise à jour “planifiée” : l’application sera mise à jour lors du passage
	// de la fenêtre de service planifiée (nuit)
	@Schema(description = "priorité", example = "1", required = false)
	// @Min(1)
	// @Max(3)
	private Integer priority;
	@Schema(description = "url", example = "https://livraison-continue.insee.fr/depot-livrable/path/to/my/zip", required = true)
	// @NotBlank
	private String url;

	@Schema(description = "numéro de la version", example = "1.0.0", required = true)
	// @NotNull
	private String version;

	@Schema(description = "type de mise à jour (variante pour les modules qui n'ont qu'un type ne pas le spécifier)", example = "1", required = false)
	private String typeMaj;

	@Schema(description = "environnement (si non spécifié prd)", example = "prd ou dmz", required = false)
	private String environnement;

	@Schema(description = "plateforme (diminutif de la plateforme )", example = "pd, pp ... a l'équipe de connaitre ses plateformes ;)", required = false)
	private String plateforme;

	@Schema(description = "mail \r\n"
			+ "    Actuellement le premier mail est renseigné avec le mail du créateur du ticket Si@moi\r\n"
			+ "    entre les identificateurs guillemets (\"\"), séparer les différents mails destinataires des notifications par un point-virgule", example = "tuSaisCeQueCUnMAil@etoui.fr;john.do@insee.fr", required = false)
	private String mail;

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public DeploymentView() {
		super();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getEnvironnement() {
		return environnement;
	}

	public void setEnvironnement(String environnement) {
		this.environnement = environnement;
	}

	public String getPlateforme() {
		return plateforme;
	}

	public void setPlateforme(String plateforme) {
		this.plateforme = plateforme;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTypeMaj() {
		return typeMaj;
	}

	public void setTypeMaj(String typeMaj) {
		this.typeMaj = typeMaj;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
