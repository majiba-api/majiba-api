package fr.insee.majiba.controller;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.mapper.ApplicationViewMapper;
import fr.insee.majiba.service.ApplicationService;
import fr.insee.majiba.view.ApplicationView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "application")
@RequestMapping("/application")
public class ApplicationController {

    @Autowired
    ApplicationService applicationService;
    @Autowired
    ApplicationViewMapper applicationViewMapper;

    @Operation(description = "chercher les applications éligibles", operationId = "applicationSearch", summary = "Chercher les applications eligible a majiba")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "search results matching criteria"),
	    @ApiResponse(responseCode = "400", description = "bad input parameter") })
    @GetMapping(value = "/search", produces = { "application/json" })
    public List<String> getApplications() {
	return applicationService.findAllNames();
    }

    @Operation(description = "chercher une application eligible et ses modules", operationId = "applicationFindOne", summary = "Chercher une application eligible a majiba avec ses modules")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "search results matching criteria"),
	    @ApiResponse(responseCode = "400", description = "bad input parameter") })
    @GetMapping(value = "/{applicationName}", produces = { "application/json" })
    public ApplicationView getApplication(
	    @Parameter(name = "applicationName", in = ParameterIn.PATH, required = false, schema = @Schema(type = "string", required = true, example = "elire")) @PathVariable("applicationName") Application application) {
	return applicationViewMapper.map(application);
    }

}
