package fr.insee.majiba.controller;

import fr.insee.majiba.domain.exception.MajibaWrongTokenException;
import fr.insee.majiba.service.load.LoadInputCsvService;
import fr.insee.majiba.view.MessageView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "config")
public class ConfigController {
    @Autowired
    LoadInputCsvService loadInputCsvService;

    @Value( "${fr.insee.majiba-api.token.admin}" )
    String adminToken;


    @Operation(description = "recharger le fichier", operationId = "loadInput", summary = "recharger le fichier")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "fichier rechargé"),
	    @ApiResponse(responseCode = "400", description = "bad input parameter") })
    @GetMapping(value = "/config/load", produces = { "application/json" })
    public MessageView configLoad(@RequestParam(name = "X-MAJIBA-API-ADMIN-TOKEN") String token) throws Exception {
        if (adminToken.equals(token)){
            this.loadInputCsvService.load();
            return new MessageView("ok");
        }
        throw new MajibaWrongTokenException(token);
    }
}
