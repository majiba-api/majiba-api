package fr.insee.majiba.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.v3.oas.annotations.tags.Tag;

@Controller
@Tag(name = "healthcheck")
@RequestMapping({ "/healthcheck" })
public class HealthcheckController {

    private static final String RETOUR_A_LA_LIGNE = System.getProperty("line.separator");

    @Value("${fr.insee.majiba-api.dossier.cei.racine}")
    private String pathMajiba;

    @Value("${fr.insee.majiba-api.fichier-entrant}")
    private String pathlistApplication;

    @Autowired
    private DataSource dataSource;

    @GetMapping
    @ResponseBody
    public String healthcheck(HttpServletResponse response) throws SQLException {
        StringBuilder sb = new StringBuilder();
        boolean listApplicationExiste = existeListApplication(pathlistApplication);
        boolean majibaExiste = existeRepertoire(pathMajiba);
        boolean bddCheck = existeDatabase();

        // sb.append("Version:" + Utils.getVersionProjet() + RETOUR_A_LA_LIGNE);
        sb.append("Présence répertoire liste des applications : " + (listApplicationExiste ? "OK" : "KO")
                + RETOUR_A_LA_LIGNE);
        sb.append("Présence répertoire de depot des demandes de maj : " + (majibaExiste ? "OK" : "KO")
                + RETOUR_A_LA_LIGNE);
        sb.append("Connexion base de donnée : " + (bddCheck ? "OK" : "KO") + RETOUR_A_LA_LIGNE);

        if (!bddCheck || !majibaExiste || !listApplicationExiste) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return sb.toString();
    }

    private boolean existeListApplication(String cheminFichier) {
        File file = new File(cheminFichier);
        if (file.exists()) {
            return true;
        }
        return false;
    }

    private boolean existeRepertoire(String repertoireApplishare) {
        Path pathTemporaire = Paths.get(repertoireApplishare);
        if (!Files.isDirectory(pathTemporaire) || StringUtils.isEmpty(repertoireApplishare)) {
            return false;
        } else {
            return true;
        }

    }

    private Boolean existeDatabase() {
        try {
            Connection connection = dataSource.getConnection();
            connection.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

}