package fr.insee.majiba.controller.technique;

import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import fr.insee.majiba.domain.exception.MajibaApplicationUnknown;
import fr.insee.majiba.domain.exception.MajibaFieldErrorException;
import fr.insee.majiba.domain.exception.MajibaWrongTokenException;
import fr.insee.majiba.view.MessageView;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MajibaControllerAdviceHighPriority {

	private final static String MESSAGE_DEFAULT = "une erreur inattendue est survenue, veuillez contacter le gestionnaire de l'application";

	@ExceptionHandler(MajibaFieldErrorException.class)
	@ResponseBody
	public ResponseEntity<MessageView> exception(MajibaFieldErrorException e) {

		final ResponseEntity<MessageView> response = new ResponseEntity<>(new MessageView(e.getMessage()),
				HttpStatus.BAD_REQUEST);
		return response;

	}

	@ExceptionHandler(MajibaWrongTokenException.class)
	@ResponseBody
	public ResponseEntity<MessageView> exception(MajibaWrongTokenException e) {

		final ResponseEntity<MessageView> response = new ResponseEntity<>(new MessageView(e.getMessage()),
				HttpStatus.BAD_REQUEST);
		return response;

	}

	@ExceptionHandler(MajibaApplicationUnknown.class)
	@ResponseBody
	public ResponseEntity<MessageView> exception(MajibaApplicationUnknown e) {

		final ResponseEntity<MessageView> response = new ResponseEntity<>(new MessageView(e.getMessage()),
				HttpStatus.BAD_REQUEST);
		return response;

	}

	@ExceptionHandler(org.springframework.web.method.annotation.MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<MessageView> conversionFailedException(MethodArgumentTypeMismatchException e) {
		e.printStackTrace();
		final Throwable rootException = ExceptionUtils.getRootCause(e);

		if (rootException.getClass().equals(MajibaApplicationUnknown.class)) {
			final String message = ((MajibaApplicationUnknown) rootException).getMessage();
			return new ResponseEntity<>(new MessageView(message), HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(new MessageView(MESSAGE_DEFAULT), HttpStatus.BAD_REQUEST);
		}
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseEntity<MessageView> exception(Exception e) {
		e.printStackTrace();
		final MessageView messsageView = new MessageView();
		messsageView.setMessage(MESSAGE_DEFAULT);
		final ResponseEntity<MessageView> response = new ResponseEntity<>(messsageView,
				HttpStatus.INTERNAL_SERVER_ERROR);
		return response;

	}

}
