package fr.insee.majiba.controller.technique;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.insee.majiba.view.MessageView;

//@ControllerAdvice
//@Order(Ordered.HIGHEST_PRECEDENCE)
public class MajibaControllerAdviceLowPriority {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<MessageView> exception(Exception e) {
	e.printStackTrace();
	final MessageView messsageView = new MessageView();
	messsageView
	.setMessage("une erreur inattendue est survenue, veuillez contacter le gestionnaire de l'application");
	final ResponseEntity<MessageView> response = new ResponseEntity<>(messsageView,
		HttpStatus.INTERNAL_SERVER_ERROR);
	return response;

    }
}
