package fr.insee.majiba.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.mapper.MajibaViewMapper;
import fr.insee.majiba.service.export.ControleTokenService;
import fr.insee.majiba.service.export.ExportDeployService;
import fr.insee.majiba.view.DeploymentView;
import fr.insee.majiba.view.MajibaView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "application")
public class DeploymentController {

    private static final Logger logger = LogManager.getLogger(DeploymentController.class);
    @Autowired
    ExportDeployService exportDeployService;
    @Autowired
    MajibaViewMapper MajibaViewMapper;
    @Autowired
    ControleTokenService controleTokenService;

    @Operation(description = "demander un déploiement", operationId = "deploy", summary = "Demander un déploiement dépose un csv")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "déploiment demandé"),
	    @ApiResponse(responseCode = "400", description = "bad input parameter") })
    @PostMapping(value = "/application/{applicationName}/deploy", consumes = "application/json", produces = { "application/json" })
    public MajibaView createDeployment(
	    @RequestBody DeploymentView deployment,
	    @Parameter(name = "applicationName", in = ParameterIn.PATH, required = true, schema = @Schema(type = "string", required = true, example = "elire")) @PathVariable("applicationName") Application application,
	    @Parameter(name = "X-MAJIBA-API-TOKEN", in = ParameterIn.HEADER, required = true) @RequestHeader(name = "X-MAJIBA-API-TOKEN") String jeton)
		    throws Exception, RuntimeException {
	logger.info("vérification du jeton");
	controleTokenService.controleToken(application, jeton);
	final MajibaView majibaView = MajibaViewMapper.mapperHandly(application, deployment);
	logger.debug(majibaView.toString());
	exportDeployService.export(majibaView);
	return majibaView;
    }
}
