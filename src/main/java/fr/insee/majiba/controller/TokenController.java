package fr.insee.majiba.controller;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.service.ApplicationService;
import fr.insee.majiba.view.TokenView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "token")
@RequestMapping("/application/")
public class TokenController {

    @Autowired
    ApplicationService applicationService;

    @Operation(description = "obtenir un token pour une application", operationId = "tokenGet", summary = "Obtenir un token pour une application, ce token doit au moins etre vérifié pour le endpoint de déploiement")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "search results matching criteria"),
	    @ApiResponse(responseCode = "400", description = "bad input parameter") })
    @GetMapping(value = "/{applicationName}/token", produces = { "application/json" })
    public TokenView getToken(
	    @Parameter(name = "applicationName", in = ParameterIn.PATH, required = false, schema = @Schema(type = "string", required = true, example = "elire")) @PathVariable("applicationName") Application application) {

	final String token = UUID.randomUUID().toString();
	application.setToken(token);
	applicationService.save(application);
	return new TokenView(token);
    }

}
