package fr.insee.majiba;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.management.MalformedObjectNameException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class MajibaApiApplication extends SpringBootServletInitializer {

	private static final Logger log = LoggerFactory.getLogger(MajibaApiApplication.class);
	private final String SPRING_NAME = "majiba-api";

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

		try {
			log.info(InetAddress.getLocalHost().getHostAddress());
			log.info(InetAddress.getLocalHost().getHostName());
		} catch (final UnknownHostException e) {
			e.printStackTrace();
		}
		return application.properties(
				"spring.config.location=classpath:/,file:${catalina.base}/webapps/" + SPRING_NAME + ".properties")
				.sources(MajibaApiApplication.class);

	}

	public static void main(String[] args)  throws UnknownHostException{
		final SpringApplication app = new SpringApplication(MajibaApiApplication.class);
		final Environment env = app.run(args).getEnvironment();
		final String serverPort = env.getProperty("server.port");
		final String applicationName = env.getProperty("spring.application.name");
		log.info(
				"\n----------------------------------------------------------\n\t"
						+ "Application '{}' is running! Access URLs:\n\t" + "Local: \t\thttp://localhost:{}\n\t"
						+ "External: \thttp://{}:{}\n\t" + "Profile(s): \t {}{}\n"
						+ "----------------------------------------------------------",
				applicationName, serverPort, InetAddress.getLocalHost().getHostAddress(), serverPort,
				env.getDefaultProfiles(), env.getActiveProfiles());
	}

}
