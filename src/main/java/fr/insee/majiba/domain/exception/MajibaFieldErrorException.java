package fr.insee.majiba.domain.exception;

public class MajibaFieldErrorException  extends Exception{

	
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// todo le message peut être modifié ofc
	public MajibaFieldErrorException(String fieldRefused) {
	      super("le champs " +fieldRefused + " n'est pas autorisé");
	   }

	   public MajibaFieldErrorException() {
	      super();
	   }
}
