package fr.insee.majiba.domain.exception;

public class MajibaWrongTokenException  extends Exception{

	
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public MajibaWrongTokenException(String tokenRefused) {
	      super("le token " +tokenRefused + " n'est pas correcte");
	   }

	   public MajibaWrongTokenException() {
	      super();
	   }
}