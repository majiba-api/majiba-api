package fr.insee.majiba.domain.exception;

public class MajibaApplicationUnknown extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MajibaApplicationUnknown(String application) {
        super("l'application " + application + " n'existe pas");
    }

    public MajibaApplicationUnknown() {
        super("L'application est inconnue");
    }

}
