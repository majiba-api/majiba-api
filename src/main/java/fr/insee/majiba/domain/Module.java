package fr.insee.majiba.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "module")
public class Module {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "type_module_id", nullable = false)
    private TypeModule typeModule;

    @ManyToOne
    @JsonIgnoreProperties("modules")
    private Application application;

    public Module() {
	super();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Application getApplication() {
	return application;
    }

    public void setApplication(Application application) {
	this.application = application;
    }

    public TypeModule getTypeModule() {
	return typeModule;
    }

    public void setTypeModule(TypeModule typeModule) {
	this.typeModule = typeModule;
    }

    @Override
    public String toString() {
	return "Module [id=" + id + ", name=" + name + ", typeModule=" + typeModule + ", application=" + application
		+ "]";
    }

}
