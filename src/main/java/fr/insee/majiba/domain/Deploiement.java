package fr.insee.majiba.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "deploiement")
public class Deploiement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Module module;

    @Column(name = "csv_location", nullable = false)
    private String csvLocation;

    public Deploiement() {
	super();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Module getModule() {
	return module;
    }

    public void setModule(Module module) {
	this.module = module;
    }

    public String getCsvLocation() {
	return csvLocation;
    }

    public void setCsvLocation(String csvLocation) {
	this.csvLocation = csvLocation;
    }

}
