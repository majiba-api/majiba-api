package fr.insee.majiba.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.repository.ApplicationRepository;

@Service
public class ApplicationService {

    @Autowired
    ApplicationRepository applicationRepository;

    public List<String> findAllNames() {
	return applicationRepository.findAllNames();
    }

    public Application findOneByBame(String name) {
	return applicationRepository.findByName(name);
    }

    public Application save(Application app) {
	return applicationRepository.save(app);
    }

    public List<Application> findAll() {
	return applicationRepository.findAll();

    }

    public void delete(Application application) {
	applicationRepository.delete(application);
    }
}
