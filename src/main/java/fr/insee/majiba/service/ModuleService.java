package fr.insee.majiba.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.insee.majiba.domain.Module;
import fr.insee.majiba.repository.ModuleRepository;

@Service
public class ModuleService {

    @Autowired
    ModuleRepository moduleRepository;

    public void saveAll(Collection<Module> modules) {
	moduleRepository.saveAll(modules);
    }

    public void save(Module module) {
	moduleRepository.save(module);
    }

    public void delete(Module module) {
	moduleRepository.delete(module);
    }
}
