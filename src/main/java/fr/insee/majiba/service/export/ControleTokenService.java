package fr.insee.majiba.service.export;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.domain.exception.MajibaApplicationUnknown;
import fr.insee.majiba.domain.exception.MajibaWrongTokenException;

@Service
public class ControleTokenService {

	
	public void controleToken ( Application appli , String token) throws MajibaWrongTokenException {
		
		if (!StringUtils.equals(appli.getToken(), token)){
			throw new MajibaWrongTokenException(token) ;
		}
	}
	
}
