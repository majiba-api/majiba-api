package fr.insee.majiba.service.export;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import fr.insee.majiba.service.UtilsService;
import fr.insee.majiba.view.MajibaView;

public class MajibaCsvService implements ExportDeployService {

    private static final Logger logger = LoggerFactory.getLogger(MajibaCsvService.class);

    private final String racineDossier;

    public MajibaCsvService(String racineDossier) {
	super();
	this.racineDossier = racineDossier;
    }

    private static final String createNameFile(String applicationName, String version, String typeMaj) {
	// ptite douille pour permettre le mock
	final LocalDateTime horodatage = UtilsService.myNow();
	final String date = horodatage.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
	final String heureMinute = horodatage.format(DateTimeFormatter.ofPattern("HH'h'mm'm'ss's'"));
	return date + "-" + heureMinute + "-" + applicationName + "-" + version + "-" + typeMaj;
    }

    private void genererCsv(MajibaView majibaView, String fileName) throws JsonGenerationException,
    JsonMappingException, IOException {
	logger.info("génération du csv dans {}", racineDossier + fileName);
	final CsvMapper mapper = new CsvMapper();

	CsvSchema schema = mapper.schemaFor(MajibaView.class).withHeader();
	mapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_EMPTY_STRINGS, true);
	mapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS, true);
	schema = schema.withColumnSeparator(';');

	// output writer
	final ObjectWriter myObjectWriter = mapper.writer(schema);
	final File tempFile = new File(racineDossier + fileName);
	myObjectWriter.writeValue(tempFile, majibaView);
    }

    @Override
    public void export(MajibaView majibaCsvView) throws JsonGenerationException, JsonMappingException, IOException {
	final String nomFichierSansExtension = createNameFile(majibaCsvView.getApplication(),
		majibaCsvView.getVersion(), majibaCsvView.getTypemaj());
	majibaCsvView.setNomFichierLog(nomFichierSansExtension + ".log");
	this.genererCsv(majibaCsvView, nomFichierSansExtension + ".csv");
	;

    }
}
