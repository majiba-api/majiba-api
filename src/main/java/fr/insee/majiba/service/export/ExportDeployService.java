package fr.insee.majiba.service.export;

import fr.insee.majiba.view.MajibaView;

public interface ExportDeployService {

    public void export(MajibaView majibaCsvView) throws Exception;

}
