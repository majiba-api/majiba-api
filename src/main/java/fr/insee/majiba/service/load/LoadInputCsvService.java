package fr.insee.majiba.service.load;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import fr.insee.majiba.MajibaProperties;
import fr.insee.majiba.domain.Application;
import fr.insee.majiba.domain.Module;
import fr.insee.majiba.domain.TypeModule;
import fr.insee.majiba.service.ApplicationService;
import fr.insee.majiba.service.ModuleService;
import fr.insee.majiba.util.ModuleUtils;

@Service
public class LoadInputCsvService {

	private static final Logger log = LogManager.getLogger(LoadInputCsvService.class);

	@Autowired
	ApplicationService applicationService;
	@Autowired
	ModuleService moduleService;
	@Autowired
	MajibaProperties majibaProperties;

	private List<InputCsvView> readFile(File csvFile) throws Exception {
		final CsvMapper mapper = new CsvMapper();
		final CsvSchema sclema = mapper.schemaFor(InputCsvView.class).withColumnSeparator(';').withoutQuoteChar()
				.withHeader();
		final MappingIterator<InputCsvView> iterator = mapper.readerFor(InputCsvView.class).with(sclema)
				.readValues(csvFile);
		return iterator.readAll();
	}

	@Transactional
	public void load() throws Exception {
		log.info("chargement du fichier {}", majibaProperties.getFichierEntrant());
		final List<InputCsvView> data = readFile(new File(majibaProperties.getFichierEntrant()));
		log.info("nbLines {}", data.size());
		final List<String> appInBase = applicationService.findAllNames();
		final Map<String, List<InputCsvView>> groupedByApp = data.stream()
				.collect(Collectors.groupingBy(InputCsvView::getApplication));
		// maj des applications existantes dans le fichier
		groupedByApp.entrySet().forEach(entry -> {
			Application app = applicationService.findOneByBame(entry.getKey());
			if (app == null) {
				log.info("creation d'une nouvelle application {}", entry.getKey());
				app = new Application();
				app.setName(entry.getKey());
				applicationService.save(app);
				createModule(app, entry.getValue());
			} else {
				log.info("maj de l'application : {}", app.getName());
				updateModule(app, entry.getValue());
				appInBase.remove(app.getName());
			}
		});
		// suppression des applications existantes en base mais non dans le
		// fichier
		for (final String app : appInBase) {
			final Application application = applicationService.findOneByBame(app);
			log.info("suppression de l'application  {} et ses modules", application.getName());
			applicationService.delete(application);
		}
	}

	private void updateModule(Application app, List<InputCsvView> lines) {
		final Map<String, Module> dbModules = mapModule(app.getModules());
		final Map<String, Module> csvModule = mapModule(app, lines);
		for (final String s : csvModule.keySet()) {
			if (dbModules.keySet().contains(s)) {
				log.info("module {} déjà existant ", s);
				dbModules.remove(s);
			} else {
				log.info("module {} a rajouté ", s);
				moduleService.save(csvModule.get(s));
			}
		}
		dbModules.values().forEach(m -> {
			moduleService.delete(m);
			log.info("suppression du module nom {}, type {}", m.getName(), m.getTypeModule().getId());
		});
	}

	/****
	 * Regroupe les lignes de csv en Map<String,Module> un module est identifié par
	 * nomDuModule+IdDeSonType Il faut donc actuellement dédoublonnes les lignes
	 * input qui présente plusieurs modules
	 *
	 * @param modules
	 * @return
	 */
	private Map<String, Module> mapModule(Application application, List<InputCsvView> lines) {
		final Map<String, Module> modulesByNameByTypeId = new HashMap<String, Module>();
		lines.forEach(line -> {
			final Module m = new Module();
			m.setName(line.getModule());
			m.setApplication(application);
			if ("1".equals(line.getMajbatch())) {
				m.setTypeModule(new TypeModule(1L));
			} else if ("1".equals(line.getMajjavascript())) {
				m.setTypeModule(new TypeModule(2L));
			} else if ("1".equals(line.getMajtomcat())) {
				m.setTypeModule(new TypeModule(3L));
			} else {
				log.warn("le module {} est skippé il n'a pas de type", m);
				return;
			}
			;
			log.debug("module {}", m);
			modulesByNameByTypeId.put(ModuleUtils.getModuleKey(m), m);
		});
		return modulesByNameByTypeId;
	}

	/****
	 * Regroupe les modules en Map<String,Modules> un module est identifié par
	 * nomDuModule+IdDeSonType
	 *
	 * @param modules
	 * @return
	 */
	private Map<String, Module> mapModule(Set<Module> modules) {
		final Map<String, Module> modulesByNameByTypeId = new HashMap<String, Module>();
		for (final Module module : modules) {
			modulesByNameByTypeId.put(ModuleUtils.getModuleKey(module), module);
		}
		return modulesByNameByTypeId;
	}

	private void createModule(Application application, List<InputCsvView> lines) {
		final Map<String, Module> csvModule = mapModule(application, lines);
		moduleService.saveAll(csvModule.values());
	}

}
