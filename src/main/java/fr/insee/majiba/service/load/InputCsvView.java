package fr.insee.majiba.service.load;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(value = { "application", "module", "majtomcat", "majbatch", "majjavascript" })
public class InputCsvView {

    @JsonProperty("application")
    private String application;
    @JsonProperty("module")
    private String module;
    @JsonProperty("majtomcat")
    private String majtomcat;
    @JsonProperty("majbatch")
    private String majbatch;
    @JsonProperty("majjavascript")
    private String majjavascript;

    public InputCsvView() {
	super();
    }

    public String getApplication() {
	return application;
    }

    public void setApplication(String application) {
	this.application = application;
    }

    public String getModule() {
	return module;
    }

    public void setModule(String module) {
	this.module = module;
    }

    public String getMajtomcat() {
	return majtomcat;
    }

    public void setMajtomcat(String majtomcat) {
	this.majtomcat = majtomcat;
    }

    public String getMajbatch() {
	return majbatch;
    }

    public void setMajbatch(String majbatch) {
	this.majbatch = majbatch;
    }

    public String getMajjavascript() {
	return majjavascript;
    }

    public void setMajjavascript(String majjavascript) {
	this.majjavascript = majjavascript;
    }

    @Override
    public String toString() {
	return "InputCsvView [application=" + application + ", module=" + module + ", majtomcat=" + majtomcat
		+ ", majbatch=" + majbatch + ", majjavascript=" + majjavascript + "]";
    }

}
