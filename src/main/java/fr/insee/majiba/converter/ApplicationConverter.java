package fr.insee.majiba.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import fr.insee.majiba.domain.Application;
import fr.insee.majiba.domain.exception.MajibaApplicationUnknown;
import fr.insee.majiba.service.ApplicationService;

@Component
public class ApplicationConverter implements Converter<String, Application> {

    @Autowired
    ApplicationService applicationService;

    @Override
    public Application convert(String name) {
    	Application appli =applicationService.findOneByBame(name);
    	if( appli == null) {
    		System.out.println("ok");
    		 throw new MajibaApplicationUnknown(name) ;
    	}
    	return appli;
    }
}