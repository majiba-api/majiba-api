package fr.insee.majiba;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties
@ConfigurationProperties(prefix = "fr.insee.majiba-api")
@Configuration
public class MajibaProperties {

    public static final String MAJIBA_API_SECURITY = "fr.insee.majiba-api.security";

    public String fichierEntrant;
    public String swaggerBasePathUrl;

    public String getFichierEntrant() {
	return fichierEntrant;
    }

    public void setFichierEntrant(String fichierEntrant) {
	this.fichierEntrant = fichierEntrant;
    }

    public String getSwaggerBasePathUrl() {
	return swaggerBasePathUrl;
    }

    public void setSwaggerBasePathUrl(String swaggerBasePathUrl) {
	this.swaggerBasePathUrl = swaggerBasePathUrl;
    }

}
