package fr.insee.majiba;

import org.junit.runner.RunWith;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(packages = "fr.insee.majiba")
public class ArchitectureTest {

    @ArchTest
    public static final ArchRule ruleService = ArchRuleDefinition.classes().that().resideInAPackage("..service..")
    .should().onlyBeAccessed()
    .byAnyPackage("..converter..", "..controller..", "..service..", "..configuration..", "..integration..");

    public static final ArchRule ruleRepository = ArchRuleDefinition.classes().that()
	    .resideInAPackage("..repository..").should().onlyBeAccessed().byAnyPackage("..service..", "..repository..");

    @ArchTest
    public static ArchRule configuration_should_be_suffixed = ArchRuleDefinition.classes().that()
    .resideInAPackage("..configuration..").should().haveSimpleNameEndingWith("Configuration");

}