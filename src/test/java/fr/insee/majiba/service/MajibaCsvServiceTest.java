//package fr.insee.majiba.service;
//
//import static org.hamcrest.core.IsEqual.equalTo;
//import static org.junit.Assert.assertThat;
//import static org.mockito.Mockito.when;
//
//import java.time.LocalDateTime;
//
//import org.junit.Ignore;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import fr.insee.majiba.MajibaApiApplication;
//import fr.insee.majiba.service.export.MajibaCsvService;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = MajibaApiApplication.class)
//@Ignore
//public class MajibaCsvServiceTest {
//
//    @InjectMocks
//    MajibaCsvService majibaCsvService;
//
//    @Mock
//    UtilsService utilsMock;
//
//    @Test
//    public void findOneByCode() {
//	when(utilsMock.myNow()).thenReturn(LocalDateTime.of(2020, 02, 28, 8, 40, 12));
//
//	final String nomFichier = majibaCsvService.createNameCsv("elire", 3, "batch");
//
//	assertThat(nomFichier, equalTo("20200228-08h40m12s-elire-3-batch.csv"));
//    }
//
//}
