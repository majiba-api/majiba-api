package fr.insee.majiba.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.insee.majiba.MajibaApiApplication;
import fr.insee.majiba.SystemPropertyActiveProfileResolver;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MajibaApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "h2", resolver = SystemPropertyActiveProfileResolver.class)
@TestPropertySource(value = "classpath:test.properties")
public class ApplicationControllerITest {

    @LocalServerPort
    private int port;
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    /******
     * Doit renvoyer une application avec ses modules
     *
     *
     * @throws Exception
     */
    @Test
    public void testGetApplicationByName() throws Exception {
        final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        final ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/application/elire"),
                HttpMethod.GET, entity, String.class);
        final String expected = "{\"nom\":\"elire\",\"modules\":[{\"code\":\"tomcat\",\"name\":\"elire-api-interne\"},{\"code\":\"tomcat\",\"name\":\"elire-api-externe\"},{\"code\":\"batch\",\"name\":\"elire-batch\"}]}";
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}