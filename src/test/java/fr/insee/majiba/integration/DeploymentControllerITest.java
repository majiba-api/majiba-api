package fr.insee.majiba.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.insee.majiba.MajibaApiApplication;
import fr.insee.majiba.SystemPropertyActiveProfileResolver;
import fr.insee.majiba.view.DeploymentView;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MajibaApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "h2", resolver = SystemPropertyActiveProfileResolver.class)
@PropertySource(value = "classpath:test.properties")
public class DeploymentControllerITest {

    @LocalServerPort
    private int port;
    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();
    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testCsv() throws Exception {
	headers.setContentType(MediaType.APPLICATION_JSON);
	headers.add("X-MAJIBA-API-TOKEN", "faketoken");
	final DeploymentView deploymentView = new DeploymentView();
	deploymentView.setEnvironnement("dmz");
	deploymentView.setModuleName("terrain");
	deploymentView.setPriority(1);
	deploymentView.setTypeMaj("javascript");
	deploymentView.setUrl("https://gforge.insee.fr/frs/download.php/file/49871/collecteprix-terrain-1.4.4.zip");
	deploymentView.setVersion("1.4.4");
	deploymentView.setMail("tutu@onse.fe");
	deploymentView.setPlateforme("pp");
	final HttpEntity<String> entity = new HttpEntity<String>(mapper.writeValueAsString(deploymentView), headers);
	final ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/application/elire/deploy"),
		HttpMethod.POST, entity, String.class);
	// assert file exists
	System.out.println(response.getBody());
    }

    private String createURLWithPort(String uri) {
	return "http://localhost:" + port + uri;
    }
}