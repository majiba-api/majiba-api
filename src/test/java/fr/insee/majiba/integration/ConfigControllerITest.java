package fr.insee.majiba.integration;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import fr.insee.majiba.MajibaApiApplication;
import fr.insee.majiba.MajibaProperties;
import fr.insee.majiba.SystemPropertyActiveProfileResolver;
import fr.insee.majiba.domain.Application;
import fr.insee.majiba.service.ApplicationService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MajibaApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "h2", resolver = SystemPropertyActiveProfileResolver.class)
@TestPropertySource(value = "classpath:test.properties")
public class ConfigControllerITest {

	@LocalServerPort
	private int port;
	TestRestTemplate restTemplate = new TestRestTemplate();
	HttpHeaders headers = new HttpHeaders();

    @Value( "${fr.insee.majiba-api.token.admin}" )
    String adminToken;

	@Autowired
	ApplicationService applicationService;
	@Autowired
	MajibaProperties majibaProperties;

	@Test
	public void testLoad() throws Exception {
		final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		majibaProperties.setFichierEntrant("./test/data/entrant/liste_majibav2_0.csv");
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/config/load"), HttpMethod.GET,
				entity, String.class);
		System.out.println(response.getBody());
		final String expected = "{\"message\":\"ok\"}";
		JSONAssert.assertEquals(expected, response.getBody(), false);
		List<Application> apps = applicationService.findAll();
		Assertions.assertThat(apps.size()).isEqualTo(4);

		majibaProperties.setFichierEntrant("./test/data/entrant/liste_majibav2_1.csv");
		response = restTemplate.exchange(createURLWithPort("/config/load"), HttpMethod.GET, entity, String.class);
		JSONAssert.assertEquals(expected, response.getBody(), false);
		apps = applicationService.findAll();
		Assertions.assertThat(apps.size()).isEqualTo(3);
	}

	private String createURLWithPort(String uri) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + uri)
        .queryParam("X-MAJIBA-API-ADMIN-TOKEN", adminToken);
		return builder.toUriString();
	}

}