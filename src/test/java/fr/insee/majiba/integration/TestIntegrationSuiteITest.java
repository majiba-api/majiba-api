package fr.insee.majiba.integration;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ApplicationControllerITest.class, DeploymentControllerITest.class, ConfigControllerITest.class })
public class TestIntegrationSuiteITest {
    // Mis si l'on veut ordonnancer les tests car certains dépendent d'autre
    // aujourd'hui l'ordre alphabétique fonctionne

}