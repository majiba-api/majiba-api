package fr.insee.majiba.configuration;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testcontainers.containers.PostgreSQLContainer;

@Configuration
@EnableJpaRepositories(basePackages = "fr.insee.majiba.repository")
@EnableTransactionManagement
@PropertySource(value = "classpath:testcontainers.properties")
public class DataSourceTestcontainersConfiguration implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceTestcontainersConfiguration.class);

    @Value("${docker.host}")
    private String dockerHost;

    @Bean
    @Profile("testcontainers-pg")
    public DataSource dataSource() {
	final PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:12.2")
		.withDatabaseName("integration-tests-db").withUsername("sa").withPassword("sa");

	postgreSQLContainer.start();

	final DriverManagerDataSource dataSource = new DriverManagerDataSource();
	dataSource.setDriverClassName("org.postgresql.Driver");
	dataSource.setUrl(postgreSQLContainer.getJdbcUrl());
	dataSource.setUsername(postgreSQLContainer.getUsername());
	dataSource.setPassword(postgreSQLContainer.getPassword());

	return dataSource;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
	logger.info("testcontainers chargement de la base ");
	logger.info("DOCKER_HOST {}", System.getenv("DOCKER_HOST"));
	logger.info("dockerhost {}", dockerHost);
	if (StringUtils.isBlank(System.getenv("DOCKER_HOST")) && StringUtils.isNotBlank(dockerHost)) {
	    logger.info("surcharge de DOCKER_HOST avec {}", dockerHost);
	    System.setProperty("DOCKER_HOST", dockerHost);
	}

    }

}
