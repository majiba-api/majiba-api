package fr.insee.majiba.util;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import fr.insee.majiba.domain.Module;
import fr.insee.majiba.domain.TypeModule;

@RunWith(JUnit4.class)
public class ModuleUtilsTest {

    @Test
    public void moduleKeyWithName() {
	final Module m = new Module();
	m.setName("name");
	m.setTypeModule(new TypeModule(1L));
	final String expected = "name1";
	Assertions.assertThat(ModuleUtils.getModuleKey(m)).isEqualTo(expected);
    }

    @Test
    public void moduleKeyWithNull() {
	final Module m = new Module();
	m.setTypeModule(new TypeModule(1L));
	final String expected = "1";
	Assertions.assertThat(ModuleUtils.getModuleKey(m)).isEqualTo(expected);
    }

    @Test
    public void moduleKeyWithBlank() {
	final Module m = new Module();
	m.setName(" ");
	m.setTypeModule(new TypeModule(1L));
	final String expected = "1";
	Assertions.assertThat(ModuleUtils.getModuleKey(m)).isEqualTo(expected);
    }
}
