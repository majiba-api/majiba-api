package fr.insee.majiba;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;

@SpringBootTest(properties = { "spring.profiles.active=h2" })
@PropertySource(value = "classpath:test.properties")
class MajibaApplicationTests {

    @Test
    void contextLoads() {
    }

}
